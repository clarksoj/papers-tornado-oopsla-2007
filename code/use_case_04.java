TaskSchedule s0 = new TaskSchedule("s0")
  .task("t0", Ex1::multiply, a, b, c)
  .task("t1", Ex1::add, c, b, d)
  .task("t2", Ex1::scale, a)
  .streamOut(d);

for(int i=0;i<numIterations;i++){
  s0.execute();
}
